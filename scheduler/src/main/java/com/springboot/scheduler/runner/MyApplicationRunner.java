
package com.springboot.scheduler.runner;

import com.springboot.scheduler.model.Student;
import com.springboot.scheduler.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Configuration
public class MyApplicationRunner implements CommandLineRunner {

    private String[] names = new String[] {"Dhiren", "Anushka", "Brad", "Andrew", "Stephane", "Ranga"};
    private String[] subjects
            = new String[] {"JS", "Java", "Spring Boot", "React", "Microservices", "Docker"};

	private StudentRepository repository;

	@Autowired
	public MyApplicationRunner(StudentRepository repository) {
		this.repository = repository;
	}

	@Override
	public void run(String... args) {
		final List<Student> students = IntStream.range(0, 100)
				.mapToObj(this::getStudent)
				.collect(Collectors.toList());
		repository.saveAll(students);
	}

	private Student getStudent(int i) {
		return new Student(randomName(new Random().nextInt(names.length)),
				randomSubject(new Random().nextInt(subjects.length)),i);
	}

	private String randomName(int position) {
		return names[position];
	}

	private String randomSubject(int position) {
		return subjects[position];
	}
}
