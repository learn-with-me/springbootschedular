package com.springboot.scheduler.repository;

import com.springboot.scheduler.model.Student;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StudentRepository extends MongoRepository<Student, String> {
}
