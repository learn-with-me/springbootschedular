package com.springboot.scheduler.controller;

import com.springboot.scheduler.model.Student;
import com.springboot.scheduler.repository.StudentRepository;
import com.springboot.scheduler.service.StudentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {

    private StudentRepository repository;
    private StudentService service;

    public StudentController(StudentRepository repository, StudentService service) {
        this.repository = repository;
        this.service = service;
    }

    @GetMapping("/students")
    public List<Student> fetchAllStudents() {
        return repository.findAll();
    }

    @PostMapping("/students/{name}/{subject}")
    public Student saveStudent(@PathVariable String name, @PathVariable String subject) {
        return service.saveAndRetrieveStudent(name, subject);
    }

}
