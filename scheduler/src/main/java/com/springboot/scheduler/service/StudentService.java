package com.springboot.scheduler.service;

import com.springboot.scheduler.model.Student;
import com.springboot.scheduler.repository.StudentRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class StudentService {

    private StudentRepository repository;

    public StudentService(StudentRepository repository) {
        this.repository = repository;
    }

    public Student saveAndRetrieveStudent(String name, String subject) {
        return repository.save(new Student(name,subject, new Random().nextInt(1000)));
    }

    private List<Student> filterNames(String name, List<Student> students) {
        return students
                .stream()
                .filter(student -> name.equalsIgnoreCase(student.getName()))
                .collect(Collectors.toList());
    }

    @Scheduled(fixedRate = 1000)
    public void runAfterCertainDelay() {
        System.out.println("Running Every Second ..");
        System.out.println("Starting and Fetching..."+ LocalDateTime.now());
        final Map<String, Long> countingBasedOnNames =
                filterNames("dhiren", repository.findAll())
                    .stream().collect(Collectors.groupingBy(Student::getName, Collectors.counting()));
        System.out.println(countingBasedOnNames);
        System.out.println("Ended...by.."+Thread.currentThread().getName() + " : " + LocalDateTime.now());
    }

}
