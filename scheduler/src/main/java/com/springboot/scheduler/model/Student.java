package com.springboot.scheduler.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document(collection = "scheduler_student")
public class Student {

    @Id
    private String id;
    private String name;
    private String subject;
    private int sequence;

    public Student(String name, String subject, int sequence) {
        this.name = name;
        this.subject = subject;
        this.sequence = sequence;
    }

}
